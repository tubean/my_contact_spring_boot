package com.tuda.mycontact.service;

import com.tuda.mycontact.entity.MyContact;
import com.tuda.mycontact.repository.MyContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MyContactServiceImpl implements MyContactService {

    @Autowired
    private MyContactRepository myContactRepository;

    @Override
    public Iterable<MyContact> findAll() {
        return myContactRepository.findAll();
    }

    @Override
    public List<MyContact> search(String q) {
        return myContactRepository.findByNameContaining(q);
    }

    @Override
    public Optional<MyContact> findOne(int id) {
        return myContactRepository.findById(id);
    }

    @Override
    public void save(MyContact contact) {
        myContactRepository.save(contact);
    }

    @Override
    public void delete(int id) {
        myContactRepository.deleteById(id);
    }
}
