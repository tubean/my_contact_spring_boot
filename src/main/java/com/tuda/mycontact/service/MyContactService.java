package com.tuda.mycontact.service;

import com.tuda.mycontact.entity.MyContact;

import java.util.List;
import java.util.Optional;

public interface MyContactService {
    Iterable<MyContact> findAll();

    List<MyContact> search(String q);

    Optional<MyContact> findOne(int id);

    void save(MyContact contact);

    void delete(int id);

}
