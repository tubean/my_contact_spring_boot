package com.tuda.mycontact.repository;

import com.tuda.mycontact.entity.MyContact;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MyContactRepository extends CrudRepository<MyContact, Integer> {

    List<MyContact> findByNameContaining(String q);

}
